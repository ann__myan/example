import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'product-list',
    loadChildren: () => import('./product-list/product-list.module').then(mod => mod.ProductListModule),
  },
  {
    path: 'counter-input',
    loadChildren: () => import('./counter/counter.module').then(mod => mod.CounterModule),
  },
  {
    path: 'two-point-range-slider',
    loadChildren: () => import('./range-slider/range-slider.module').then(mod => mod.RangeSliderModule)
  },
  {
    path: 'range-slider-version2',
    loadChildren: () => import('./range-slider-version2/range-slider-version2.module').then(mod => mod.RangeSliderVersion2Module)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'product-list',
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
