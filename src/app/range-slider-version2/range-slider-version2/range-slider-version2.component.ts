import { 
  Component,
  HostListener,
  ElementRef,
  ViewChild,
  AfterViewInit,
  Input,
  OnInit
} from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-range-slider-version2',
  templateUrl: './range-slider-version2.component.html',
  styleUrls: ['./range-slider-version2.component.css']
})
export class RangeSliderVersion2Component implements OnInit, AfterViewInit {
  @Input() minValue: number = 0;
  @Input() maxValue: number = 100;
  firstThumbValue: number;
  secondThumbValue: number;

  @ViewChild('firstThumb', { static: false })
  firstThumbElemRef: ElementRef;
  @ViewChild('secondThumb', { static: false })
  secondThumbElemRef: ElementRef;
  trackElement: HTMLElement;

  firstThumbWidth: number;
  secondThumbWidth: number;
  thumbWidthPx: number;
  trackWidthPx: number;
  firstLeftPx: number = 0;
  secondLeftPx: number = 0;
  minThumbClickedDown: boolean = false;
  maxThumbClickedDown: boolean = false;
  clickedElem: HTMLDivElement;

  ngOnInit(): void {
    this.firstThumbValue = this.minValue;
    this.secondThumbValue = this.maxValue;
  }

  ngAfterViewInit(): void {
    this.thumbWidthPx = this.secondThumbElemRef.nativeElement.offsetWidth;
    this.trackElement = this.firstThumbElemRef.nativeElement.parentElement;
    this.trackWidthPx = this.trackElement.offsetWidth - this.thumbWidthPx;
    this.getThumbWidths();

    timer().subscribe(() => {
      this.secondLeftPx = this.trackElement.offsetWidth - this.thumbWidthPx;
    });
  }

  downEvent(event: MouseEvent): void {
    this.clickedElem = event.target as HTMLDivElement;
  }

  private getThumbWidths(): void {
    this.secondThumbWidth = this.secondThumbElemRef.nativeElement.offsetWidth;
    this.firstThumbWidth = this.firstThumbElemRef.nativeElement.offsetWidth;
  }

  @HostListener('window:mousemove', ['$event'])
  onMouseMove(event: MouseEvent): void {
    if(!this.clickedElem) { return; }

    this.getThumbWidths();

    const trackLeft = this.trackElement.offsetLeft;
    const trackRigth = this.trackElement.offsetWidth + trackLeft - this.thumbWidthPx;
    const mouseLeft = event.clientX;
    let positionVal: number;

    if (mouseLeft < trackLeft) {
      positionVal = 0;
    } else if (mouseLeft > trackRigth) {
      positionVal = trackRigth - trackLeft;
    } else {
      positionVal = mouseLeft - trackLeft;
    }

    if (this.clickedElem === this.firstThumbElemRef.nativeElement) {
      this.firstLeftPx = positionVal;
    }
    if (this.clickedElem === this.secondThumbElemRef.nativeElement) {
      this.secondLeftPx = positionVal;
    }

    this.calculateValue();
  }

  private calculateValue(): void {
    const deltaValue = this.maxValue - this.minValue;
    const onePxPoints = deltaValue / this.trackWidthPx;

    if (this.clickedElem === this.firstThumbElemRef.nativeElement) {
      this.firstThumbValue = this.minValue + onePxPoints * this.firstLeftPx;
    }
    if (this.clickedElem === this.secondThumbElemRef.nativeElement) {
      this.secondThumbValue = this.minValue + onePxPoints * this.secondLeftPx;
    }
  }

  @HostListener('window:mouseup')
  mouseup(): void {
    if(!this.clickedElem) { return; }
    this.clickedElem = null;
  }

}
