import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RangeSliderVersion2Component } from './range-slider-version2/range-slider-version2.component';
import { RangeSliderVersion2RoutingModule } from './range-slider-version2-routing.module';
import { RangeSliderVersion2PageComponent } from './range-slider-version2-page/range-slider-version2-page.component';



@NgModule({
  declarations: [
    RangeSliderVersion2Component,
    RangeSliderVersion2PageComponent,
  ],
  imports: [
    CommonModule,
    RangeSliderVersion2RoutingModule,
  ]
})
export class RangeSliderVersion2Module { }
