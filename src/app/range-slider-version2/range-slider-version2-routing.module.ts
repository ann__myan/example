import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RangeSliderVersion2PageComponent } from './range-slider-version2-page/range-slider-version2-page.component';

const routes: Routes = [
  {
    path: '',
    component: RangeSliderVersion2PageComponent,
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class RangeSliderVersion2RoutingModule {

}