import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CounterInputElementComponent } from './counter-input-element/counter-input-element.component';

const routes: Routes = [
  {
    path: '',
    component: CounterInputElementComponent
  },
];

@NgModule({
  declarations: [ ],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CounterRoutingModule {

}