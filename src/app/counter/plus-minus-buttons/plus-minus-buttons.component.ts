import { Component, OnInit } from '@angular/core';
import { CounterService } from '../../services/counter.service';

@Component({
  selector: 'app-plus-minus-buttons',
  templateUrl: './plus-minus-buttons.component.html',
  styleUrls: ['./plus-minus-buttons.component.css']
})
export class PlusMinusButtonsComponent implements OnInit {

  constructor(
    private counterService: CounterService,
  ) { }

  ngOnInit() { }

  onPlus() {
    this.counterService.counterSubject.next(
      this.counterService.counterSubject.getValue() + 5
    );
  }

  onMinus() {
    this.counterService.counterSubject.next(
      this.counterService.counterSubject.getValue() - 7
    );
  }
}
