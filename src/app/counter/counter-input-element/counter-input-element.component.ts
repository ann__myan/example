import { Component, OnInit } from '@angular/core';
import { CounterService } from '../../services/counter.service';

@Component({
  selector: 'app-counter-input-element',
  templateUrl: './counter-input-element.component.html',
  styleUrls: ['./counter-input-element.component.css']
})
export class CounterInputElementComponent implements OnInit{
  counterProp: number;

  constructor(
    private counterService: CounterService,
  ) { }

  ngOnInit(): void {
    this.counterService.counterSubject.subscribe((val: number) => {
      this.counterProp = val;
    });
  }

  changeVal(val: string): void {
    this.counterService.counterSubject.next(+val);
  }
}