import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CounterRoutingModule } from './counter-routing.module';
import { CounterInputElementComponent } from './counter-input-element/counter-input-element.component';
import { PlusMinusButtonsComponent } from './plus-minus-buttons/plus-minus-buttons.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CounterInputElementComponent,
    PlusMinusButtonsComponent,
  ],
  imports: [
    CommonModule,
    CounterRoutingModule,
    FormsModule,
  ]
})
export class CounterModule {

}