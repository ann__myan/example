import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { products } from '../../products';
import { ActiveButtonService } from 'src/app/services/active-button.service';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit{
    product;
    pathName: string = "product-list";

    constructor (
        private route : ActivatedRoute,
    ) { }

    ngOnInit( ) {
        this.route.paramMap.subscribe(params => {
            this.product = products[+params.get('productId')];
        })
    }
}