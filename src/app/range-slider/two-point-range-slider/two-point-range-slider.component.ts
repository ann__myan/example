import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-two-point-range-slider',
  templateUrl: './two-point-range-slider.component.html',
  styleUrls: ['./two-point-range-slider.component.css']
})
export class TwoPointRangeSliderComponent implements OnInit {
  @Input() minValue: number = 0;
  @Input() maxValue: number = 100;
  @Input() minRangeProp: number;
  @Input() maxRangeProp: number;
  @Input() step: number = 1;

  shownMinProp: number;
  shownMaxProp: number;

  constructor() { }

  ngOnInit() {
    this.detectShownValues();
  }

  private detectShownValues(): void {
    if (this.minRangeProp > this.maxRangeProp) {
      this.shownMinProp = this.maxRangeProp;
      this.shownMaxProp = this.minRangeProp;
    } else {
      this.shownMinProp = this.minRangeProp;
      this.shownMaxProp = this.maxRangeProp;
    }
  }

  onFirstSliderMove(value: number): void {
    this.minRangeProp = value;
    this.detectShownValues();
  }

  onSecondSliderMove(value: number): void {
    this.maxRangeProp = value;
    this.detectShownValues();
  }
}
