import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RangeSliderPageComponent } from './range-slider-page/range-slider-page.component';

const routes: Routes = [
  {
    path: '',
    component: RangeSliderPageComponent,
  }
];

@NgModule({
  declarations: [ ],
  imports: [
    RouterModule.forChild(routes)
   ],
  exports: [ 
    RouterModule
  ]
})
export class RangeSliderRountingModule {
  
}