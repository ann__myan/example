import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwoPointRangeSliderComponent } from './two-point-range-slider/two-point-range-slider.component';
import { RangeSliderRountingModule } from './range-slider-routing.module';
import { FormsModule } from '@angular/forms';
import { RangeSliderPageComponent } from './range-slider-page/range-slider-page.component';



@NgModule({
  declarations: [
    TwoPointRangeSliderComponent,
    RangeSliderPageComponent,
  ],
  imports: [
    CommonModule,
    RangeSliderRountingModule,
    FormsModule,
  ]
})
export class RangeSliderModule { }
