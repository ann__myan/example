import { Component } from '@angular/core';
import { ActiveButtonService } from 'src/app/services/active-button.service';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.css']
})
export class MenuBarComponent {
  opened: boolean = true;

  constructor (  ) { }

  toggleOpenState() {
    this.opened = !this.opened;
  }
}