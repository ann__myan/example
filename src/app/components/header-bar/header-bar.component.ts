import { Component, OnInit } from '@angular/core';
import { ActiveButtonService } from 'src/app/services/active-button.service';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.css']
})
export class HeaderBarComponent implements OnInit{

  constructor ( ) { }

   ngOnInit() { }
}